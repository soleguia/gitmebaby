import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginView from '@/views/LoginView.vue'
import HomeView from '@/views/HomeView.vue'
import ContributorView from '@/views/ContributorView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'HomeView',
    component: HomeView
  },
  {
    path: '/login',
    name: 'LoginView',
    component: LoginView
  },
  {
    path: '/repo/:repoOwner/:repoName',
    name: 'RepoView',
    component: HomeView,
    props: true
  },
  {
    path: '/contributor/:login',
    name: 'ContributorView',
    component: ContributorView
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  let logged_in = localStorage.getItem("logged_in") === 'true' ? true : false;  
  if(logged_in || (!logged_in && to.name === 'LoginView')){
    next();
  } else {
    next({name: 'LoginView'});
  }

});

export default router
